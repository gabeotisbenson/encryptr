import { Context as KoaContext } from 'koa';
import Router from 'koa-router';

const router = new Router();

router.get('/', (ctx: KoaContext): void => {
	ctx.body = 'Hello World';
});

export default router;
