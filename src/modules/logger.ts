import consola from 'consola';

type BasicLoggingFunction = (msg: string) => void;
type ErrorLoggingFunction = (msg: string, err?: Error) => void;

type LoggerConstructorObject = {
	error: ErrorLoggingFunction;
	info: BasicLoggingFunction;
	log: BasicLoggingFunction;
	success: BasicLoggingFunction;
	warn: BasicLoggingFunction;
};

class Logger {
	public error: ErrorLoggingFunction;
	public info: BasicLoggingFunction;
	public log: BasicLoggingFunction;
	public success: BasicLoggingFunction;
	public warn: BasicLoggingFunction;

	public constructor ({ error, info, log, success, warn }: Readonly<LoggerConstructorObject>) {
		this.error = error;
		this.info = info;
		this.log = log;
		this.success = success;
		this.warn = warn;
	}
}

const logger = new Logger(consola);

export default logger;
