import { RESPONSE_TIME } from '~/globals/headers';
import {
	Context as KoaContext,
	Next as KoaNext
} from 'koa';

export const responseTime = async (ctx: KoaContext, next: KoaNext): Promise<void> => {
	const start = Date.now();
	await next();
	const ms = Date.now() - start;
	ctx.set(RESPONSE_TIME, `${ms}ms`);
};
