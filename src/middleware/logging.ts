import logger from '~/modules/logger';
import { RESPONSE_TIME } from '~/globals/headers';
import {
	Context as KoaContext,
	Next as KoaNext
} from 'koa';

export const responseTimeLogging = async (ctx: KoaContext, next: KoaNext): Promise<void> => {
	await next();
	const rt = ctx.response.get(RESPONSE_TIME);
	logger.info(`${ctx.method} ${ctx.url} - ${rt}`);
};
