import Koa from 'koa';
import logger from '~/modules/logger';
import { responseTime } from '~/middleware/response-time';
import { responseTimeLogging } from '~/middleware/logging';
import router from '~/modules/router';

const PORT = 3000;
const app = new Koa();

app
	.use(responseTimeLogging)
	.use(responseTime)
	.use(router.routes())
	.use(router.allowedMethods());

app.listen(PORT, () => logger.success(`API listening on port ${PORT}`));
